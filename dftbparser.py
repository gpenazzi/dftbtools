# -*- coding: utf-8 -*-
"""
Created on Sun Mar 25 21:33:02 2012
This Module will contain functions to parse DFTB output files and
return native or numpy data types

@author: gpenazzi
"""

import sys
import os
import math
import numpy as np


def get_fermi(units = 'eV', filename = 'detailed.out'):
    """Get Fermi level"""
    fermi_level = 0.0
    for line in open(filename, 'r'):
        words = line.split()
        if len(words) < 6:
            continue
        if words[0] == 'Fermi' and words[1] == 'energy:':
            if units == 'H':
                fermi_level = float(words[-4])
            elif units == 'eV':
                fermi_level =  float(words[-2])
    return fermi_level


def get_charges(filename = 'detailed.out'):
    """Read atom resolved mulliken charges
    from detailed.out and return a list"""
    charges = list()
    datafile = open(filename, 'r')
    for line in datafile:
        words = line.split()
        if len(words) == 0:
            continue
        if (words[0] == 'Atom'
        and words[1] == 'Net' and words[2] == 'charge'):
            break
    for line in datafile:
        words = line.split()
        if len(words) == 0:
            break
        else:
            charges.append(float(words[1]))
    return charges

def write_charges(positions, filename = 'charges.dat', ind = None):
    """Write charges read from file. Output format is:
    x y z charge(-e units)
    If an array of indexes is specified, only the corresponding
    charge is considered.
    positions must be a Nx3 array (or equivalent)"""
    charges = get_charges()
    with open(filename, 'w') as output_file:
        if ind == None:
            ind = range(len(charges))
        for i in ind:
            output_file.write('%20.15f %20.15f %20.15f %20.15f\n' %
            (positions[i][0], positions[i][1], positions[i][2], charges[i]))


def get_eigenvalues(units = 'eV', filename = 'detailed.out'):
    """Get eigenvalues"""
    eigenvalues = list()
    datafile = open(filename, 'r')
    for line in datafile:
        words = line.split()
        if len(words) == 0:
            continue
        if (words[0] == 'Eigenvalues'
        and words[1] == '/eV' and units == 'eV'):
            break
        if (words[0] == 'Eigenvalues'
        and words[1] == '/H' and units == 'H'):
            break
    for line in datafile:
        words = line.split()
        if len(words) == 0:
            break
        else:
            eigenvalues.append(float(words[0]))
    datafile.close()
    return eigenvalues


def get_transmission(units = 'eV', filename = 'detailed.out'):
    """Get transmission from detailed.out as list.
    One list return energy point, the other returns energy values"""
    if units != 'eV':
        sys.exit('get_transmission supports only eV')
    energies = list()
    trans = list()
    with open(filename) as filedata:
        lines = filedata.readlines();
        for x, line in enumerate(lines):
            words = line.split()
            if len(words) == 0:
                continue
            if (words[0] == 'Total'
            and words[1] == 'Transmission'):
                break
        for line in lines[(x + 2):]:
            words = line.split()
            if len(words) == 0:
                break
            else:
                energies.append(float(words[0]))
                trans.append(float(words[1]))
    return energies, trans


def get_modes(modes = None, filename = 'modes.xyz'):
    """ Read eigenvector of vibrational mode specified by the list
    of integer modes. Return a dictionary, keys are the index of the mode,
    values are 2ples containing frequency in cm-1
    and eigenvector as a 2D array. I assume that modes.xyz is well
    formatted (no additional spacing etc.) to save parsing time.
    IMPORTANT: modes are numbered as in the file, i.e. fortran indexing
    
    Parameters
    ----------
    modes : list of integer
            Contains the list of modes that will be returned.
            If not specified, all modes are returned.
    filename: string
            File containing modes. The syntax is taken from the output of
            modes_1.2.
            
    Returns
    -------
    modes_dict: a dictionary with the integer labelling the mode as keys.
            items are 2-value lists [frequency, eigenvector], where 
            frequency is a float and eigenvector is a numpy.array"""

    modes_dict = dict()

    with open(filename) as filedata:
        lines = filedata.readlines()
        natoms = int(lines[0].split()[0])

        if modes == None:
            nmodes = len(lines) // (natoms + 2)
            assert lines[1].split()[0] == 'Eigenmode'
            first_mode = int(lines[1].split()[1])
            modes = range(first_mode, first_mode + nmodes)

        for mode_index in modes:
            #Look for the right starting point, skip lines with vectors to be
            #faster
            mode_position = -1
            for index, line in enumerate(lines[1::natoms + 2]):
                entries = line.split()
                assert entries[0] == 'Eigenmode'
                if int(entries[1]) == mode_index:
                    mode_position = index
                    break
            if mode_position == -1:
                raise ValueError('Cannot find specified mode in modes file')

            start_index = (2 + natoms) * (mode_position) + 1
            "Fill single mode data"
            mode_data = [0.0,
                         np.zeros((natoms, 3), dtype=np.float64)]
            entries = lines[start_index].split()
            assert int(entries[1]) == mode_index
            #Frequency
            mode_data[0] = np.float64(entries[2])
            #Vector
            for atm_ind, line in enumerate(
                      lines[(start_index + 1):(start_index + natoms + 1)]):
                entries = line.split()
                mode_data[1][atm_ind,:] = np.float64(entries[5:])
                modes_dict[mode_index] = mode_data

    return modes_dict




def get_hs_sqr(kpoint = 1, units = 'Hartree', filename = 'hamsqr1.dat'):
    """Read H or S in square folded DFTB format
    Returns a numpy 2D array, complex or real depending on the
    flag in the file. Up to now it works for a single k point specified
    by an integer."""

    print("Reading RealHS")

    with open(filename) as filedata:

        lines = filedata.readlines();
        #Header information are on the second line
        entries = lines[1].split()
        assert len(entries) == 3
        #Extract matrix information
        if entries[0] == 'T':
            datatype = np.float64
        elif entries[0] == 'F':
            datatype = np.complex128
        else:
            raise ValueError('Error in hamil1sqr.dat or overlsqr.dat.'
            'REAL is not F or T')
        norb = int(entries[1])
        nkpoint = int(entries[2])
        if kpoint > nkpoint:
            raise ValueError('Error in get_hs_sqr. kpoint not found.')
        #Create 2D array for the return value
        hs = np.zeros((norb, norb), dtype=datatype)

        #Scan the file and look for the right line index
        for index, line in enumerate(lines):
            entries = line.split()
            if len(entries) > 1:
                if line.split()[1] == 'IKPOINT' \
                   and int(lines[index + 1].split()[0]) == kpoint:
                   start_index = index + 3
        #Now parse the values
        for index, line in enumerate(lines[start_index:start_index + norb]):
            #We parse everything as float, at first
            parsed_array = np.array(line.split(), dtype=np.float64)
            #for real matrix, this does already the job
            if datatype == np.float64:
                line_array = parsed_array
            #For complex matrices, we need this tricky line to split
            elif datatype == np.complex128:
                line_array = np.array(  \
                parsed_array[0::2],dtype=np.complex128)+1j*np.array(  \
                parsed_array[1::2],dtype=np.complex128)
                print(line_array)

            hs[index,:] = line_array

    #Unit conversion, to be done nicer
    if units == 'eV':
        hs = hs * 27.2116

    return hs

def get_hs_real(filename = 'hamreal1.dat'):
    """Read H or S in real-space sparse DFTB format
    Returns dictionary indexed with the following t-uples:
        h[(atm1_ind, orb1, atm2_ind,  orb2)] = value"""

    print("Reading RealHS")

    datafile = open(filename, 'r')
    mat = dict()
    line = datafile.readline()
    while line:
        words = line.split()
        #A block starts here
        if len(words) < 4:
            line = datafile.readline()
            continue
        if (words[1] == 'IATOM1' and words[2] == 'INEIGH' and
        words[3] == 'IATOM2F'):
            line = datafile.readline()
            words = line.split()
            iatm1 = int(words[0]) - 1
            #ineigh = int(words[1])
            iatm2 = int(words[2]) - 1
            #Read values entries
            orb1 = 0
            line = datafile.readline() #MATRIX
            line = datafile.readline()
            words = line.split()
            while not (words[0] == '#'):
                prev = datafile.tell()
                for orb2, str_val in enumerate(words):
                    dict_ind = (iatm1, orb1, iatm2, orb2)
                    mat[dict_ind] = float(str_val)
                    #WARNING: true if hamiltonian real, otherkwise this is
                    #the complex conjugate
                    dict_ind = (iatm2, orb2, iatm1, orb1)
                    val = float(str_val)
                    if math.isnan(val):
                        print('Error: NaN while parsing ', dict_ind)
                        sys.exit('Error')
                    mat[dict_ind] = val
                orb1 = orb1 + 1
                line = datafile.readline()
                if not line:
                    break
                else:
                    words = line.split()
            datafile.seek(prev)

        line = datafile.readline()

    print("Done, 1st is ", mat[(0, 0, 0, 0)])

    datafile.close()
    return mat

def reduce_eigenvector_file(ind_min, ind_max, filename = 'eigenvec.out'):
    """Substitute eigenvector file with one containing only eigenvecors
    in the range ind_min, ind_max. This is needed when the system is
    quite big and we don't want to keep some giant file with all the
    eigenvectors in memory"""
    start = 0
    end = 0
    with open(filename, 'r') as datafile:
        if (ind_min > ind_max):
            sys.exit('Error in '
            + 'reduce_eigenvector_file(): ind_min > imd_max')
        for line in datafile:
            words = line.replace(':', ' ').split()
            if len(words) == 0:
                continue
            if (words[0] == 'Eigenvector'
            and words[1] == str(ind_min - 1)):
                start = datafile.tell()
            if (words[0] == 'Eigenvector'
            and words[1] == str(ind_max + 1)):
                end = datafile.tell()

        #Store lines in a temporary string
        size = end - start
        if size == 0:
            return -1
        datafile.seek(start)
        content = datafile.read(size)

    #Overwrite content
    with open(filename, 'w') as datafile:
        datafile.write(content)

    #If existing, delete "eigenvector.bin"
    if os.path.isfile('eigenvec.bin'):
        os.remove('eigenvec.bin')



def get_eigenvector(index, filename = 'eigenvec.out'):
    """Get eigenvector specified by index. A list of list is returned,
    where the first index goes through the atoms and the second trough the
    orbitals. """
    datafile = open(filename, 'r')
    eigenvector = list()
    for line in datafile:
        #Replace is needed because sometimes there's no whitespace after
        #"Eigenvector:"
        words = line.replace(':', ' ').split()
        if len(words) == 0:
            continue
        if (words[0] == 'Eigenvector'
        and words[1] == str(index)):
            #print 'Checking eigenvector', index
            break
    orbitals = list()
    for line in datafile:
        words = line.replace(':', ' ').split()
        if len(words) == 0:
            if not(len(orbitals) == 0):
                eigenvector.append(orbitals)
            orbitals = list()
            continue
        elif (words[0] == 'Eigenvector'):
            break
        else:
            orbitals.append(float(words[1]))

    datafile.close()
    #print "eigenvector is long ", len(eigenvector)
    #print "first eigenvector element is ", len(eigenvector[0])
    return eigenvector



def get_homo_lumo(units = 'eV', filename = 'detailed.out'):
    """Get HOMO level"""
    eigenvalues = get_eigenvalues(units, filename)
    fermi_level = get_fermi(units, filename)
    index_homo = 0
    index_lumo = 0
    #Assume that eigenvalues are ordered, as they are ordered in
    #detailed.out file
    for eigenvalue in eigenvalues:
        if eigenvalue > fermi_level:
            index_lumo = eigenvalues.index(eigenvalue)
            index_homo = index_lumo - 1
            break

    return [eigenvalues[index_homo], eigenvalues[index_lumo]]
