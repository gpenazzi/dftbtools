# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 10:25:38 2013

@author: Gabriele Penazzi

This module contains function and data structure useful for 
calculationg electron phonon coupling in DFTB+, using frozen 
phonon approximation

"""

#This will be dropped: see _gen_to_array
import ase
from ase import io
import dftbtools
from dftbtools.parsing import outputparser
from subprocess import call
import shutil
import os
import numpy as np
import tempfile
    

def _gen_to_array(filename):
    """
    Return a 2d array [atm][coord] with atom coordinates from a gen file.
    This is wrapped here as I use ASE for convenience but I want to keep
    this dependency wrapped, I will implement a separated function to 
    drop the dependancy.
    """

    structure = ase.io.read(filename)
    return structure.get_positions()


def _gen_change_coords(inputfile, outputfile, coords):
    """
    Take an atomistic structure stored in file specified in <inputfile>
    and write it to <outputfile>, changing the coordinates according
    to the array <coords>. This is also wrapped here as it temporary
    depends upon ASE
    """
    structure = ase.io.read(inputfile, format = 'gen')
    structure.set_positions(coords)
    structure.write(outputfile, format = 'gen')
    return 0
    


#TODO: I just started and there's already a TODO....Many parameters
#can be deduces from the dftb_in.hsd file but I don't want to put some dirty 
#parsing here because I want to write a specific python module for that. 
#Therefore some of them are redundant

class ElPhCoupling():
    """ A class to manage the calculation of electron-phonon coupling
    for DFTB+NEGF calculation with inelastic scattering (IETS spectra) """
    
    def __init__(self, options):
        """ Class initializer. All the parameters can be assigned here.
        In methods, all None options are assigned from private member,
        if existing.
                   
        """

        self._options = options

#        self._inputfile = os.path.basename(inputfile)
#        self._structfile = os.path.basename(structfile)
        self._eq_h = None
        self._eq_s = None
        self._eq_invs = None
        self._modes_dict = None

    def _default_options(self):
        """Return a dictionary with default options. To be used in the 
        contructor. It also works as a reference for all the options.
        Meaningless defalt are set to None."""
        options = dict()
        options['executable'] = 'dftb+'
        options['inputfile'] = 'dftb_in.hsd'
        options['structfile'] = None
        options['modesfile'] = 'modes.xyz'
        options['removeasap'] = True
        options['saveformat'] = 'txt'
        options['dr'] = 0.01
        options['workdir'] = dict()
        options['modes'] = None
        options['modesfile'] = 'modes.xyz'
        #Decorate the mode array with an integer
        #quantity of zeros array. This is useful when the modes are 
        #actually calculated on a subsystems respect to the 
        #system we want to calculate the coupling on
        options['decorate'] = 0
        options['initialcharge'] = None

        return options


    def init(self):
        """
        Initilization procedures.
        """
        #Assign default options and override them with user defined options
        options = self._default_options()
        options.update(self._options)
        self._options = options

        self._workdir = self._options['workdir']
        for key in self._workdir:
            self._workdir[key] = os.path.abspath(self._workdir[key])
        self._parse_modes_dict()
        if options['initialcharge'] != None:
            options['initialcharge'] = \
              os.path.abspath(options['initialcharge'])

    def _write_coupling(self, coupling, mode):
        """
        Write coupling to file
        """
        saveformat = self._options['saveformat']
        
        prefix = 'elph_m' + str(mode) + '.'
        if saveformat == 'txt':
            filename = prefix + 'txt'
            np.savetxt(filename, coupling)
        if saveformat == 'bin':
            filename = prefix = 'bin'
            np.save(filename, coupling)

    def run(self):
        """
        Run all the calculations needed for the el-ph coupling.
        """

        modes = self._options['modes']
        removeasap = self._options['removeasap']

        #Calculate 
        print('Calculating el-ph coupling')
        for mode in modes:

            if mode == 0:
                self._run_mode0(
                chargefile = self._options['initialcharge'])
                self._options['initialcharge'] = \
                  self._workdir[0] + '/charges.bin'
            if mode != 0:
                self._run_mode(mode, removeasap, 
                chargefile = self._options['initialcharge'])

        #When everything is done, we can also remove the 
        #equilibrium calculation
        if removeasap:
                 shutil.rmtree(self._workdir[0])
        print('Done')


    def _run_mode0(self, chargefile = None):
        """
        Run the equilibrium calculation. The working directory is not 
        deleted, as it will be used by all the coupling calculations.
        The associated matrices are immediately parsed.
        """
        path = self._run_mode_displaced_dftb(mode = 0, chargefile = chargefile)
        self._read_eq_hs()
        self._eq_invs = np.linalg.inv(self._eq_s)
        return 0

    def _run_mode(self, mode, removeasap = None, chargefile = None):
        """
        Run any mode hamiltonian and overlap calculation.
        """
        self._run_mode_displaced_dftb(mode = mode, chargefile = chargefile)
        coupling = self.calculate_coupling(mode)
        self._write_coupling(coupling, mode)
        if removeasap == True:
             shutil.rmtree(self._workdir[mode])
        return 0

    def _displace_gen(self, displ, inputfile, outputfile):
        """
        Create a input gen with displaced atoms. 

        """
        
        dr = self._options['dr']
        
        positions = _gen_to_array(inputfile)
        if len(positions) != len(displ):
            raise ValueError('Error in elcoupl._displace_gen(): positions '
                            'and displ must have the same length')

        positions = positions + dr * displ
        print('Printing updated positions to', outputfile)
        _gen_change_coords(inputfile, outputfile, positions)
        return 0

    def _run_mode_displaced_dftb(self, mode, chargefile = None):
        """
        Modify dftb input file and structure file to run two dftb+ calculations.
        The first one calculate the charges, the second write the Hamiltonian
        and Overlap matrix.
        Return the absolute working path.
        """

        dftbinput = self._options['inputfile']
        structfile = self._options['structfile']
        executable = self._options['executable']

        #Create a working directory for the specific mode. If the
        #directory already exists, we assume that also the right data
        #is there and we skip the calculation
        current = os.getcwd()
        if mode in self._workdir:
            print('Mode',mode,' provided by the user')
            return self._workdir[mode]
        else:
            self._workdir[mode] = tempfile.mkdtemp('_m' + str(mode), dir = current)

        workdir = self._workdir[mode]

        #copy the file needed in the temporary directory and go there
        shutil.copy(dftbinput, workdir)
        shutil.copy(structfile, workdir)
        os.chdir(workdir)
        print('Here chargefile',chargefile)
        if chargefile:
            self._dftb_find_replace_line('ReadInitialCharges', 'ReadInitialCharges = Yes')
            print('Changed dftb_in')
            shutil.copy(chargefile, workdir)

        #First run dftb to get the convergent charges
        #If a charge file is available, set the appropriate option
        #and copy the file here
        os.chdir(workdir)
        if mode == 0:
            tmp_dftbinput = dftbinput + str(mode)
        else:
            tmp_structfile =  structfile + '.' + str(mode)
            self._displace_gen(self._modes_dict[mode][1], structfile, 
            tmp_structfile)          
            err = self._dftb_find_replace_line(structfile, 
            '<<< " ' + tmp_structfile + '"')

        #Run calculation
        call(executable)
        #Set WriteHS and ReadInitialCharges on right value and run again
        self._dftb_find_replace_line('ReadInitialCharges', 'ReadInitialCharges = Yes')
        self._dftb_find_replace_line('WriteHS', 'WriteHS = Yes')
        call(executable)

        hampath = workdir + '/hamsqr1.dat'
        spath = workdir + '/oversqr.dat'

        #Go back to starting directory
        os.chdir(current)

        return 0


    def calculate_coupling(self, mode, dr = None, overlap = True):
        """
        Calculate el-ph coupling. If parameter are not specified, default internal 
        names are used
        """

        print(self._workdir)
        print(mode)
        hamfile = self._workdir[mode] + '/hamsqr1.dat'
        overfile = self._workdir[mode] + '/oversqr.dat'
        modefreq = self._modes_dict[mode][0]
        dr = self._options['dr'] 

        ham = outputparser.get_hs_sqr(filename = hamfile)
        over = outputparser.get_hs_sqr(filename = overfile)

        if modefreq <= 0.0:
            raise ValueError('ERROR: negative frequency in mode', mode)
        #cm-1 -> Hartree: 100/(2 * Rydberg)
        omega = (100.0 / (2.0 * 10973731.568527)) * modefreq
        prefactor = np.sqrt(1.0 / (2.0 * omega * 1822))

        gradham = (ham - self._eq_h) / dr
        del(ham)
        gradover = (over - self._eq_s) / dr
        del(over)

        coupling = np.copy(gradham)
        del(gradham)
        if overlap:
            work1 = np.dot(gradover, self._eq_invs)
            work2 = np.dot(work1, self._eq_h)
            coupling = coupling - 0.5 * work2
            work1 = np.dot(self._eq_h, self._eq_invs)
            work2 = np.dot(work1, gradover)
            del(work1)
            coupling = (coupling - 0.5 * work2) * prefactor
            del(work2)
        else:
            coupling = coupling * prefactor

        return coupling




    def _parse_modes_dict(self):
        """
        Parse modes specified in list modes and save them to a class member.
        """
        modes = self._options['modes']
        filename = self._options['modesfile']
        decorate = self._options['decorate']

        #The fact that I label the equilibrium calculation as mode 0 could
        #make some confusion in the parser, take that away from the call
        modes_wo_zero = [x for x in modes if x is not 0]
        self._modes_dict = outputparser.get_modes(modes_wo_zero, filename)
        #If any decoration specified, put it directly here
        if decorate == 0:
            return 0
        else:
            decoration = np.zeros((decorate, 3))
            for key in self._modes_dict:
                self._modes_dict[key][1] = np.vstack(  
                 (self._modes_dict[key][1], decoration)) 
            return 0



    def _dftb_find_replace_line(self, keyword, updateline):
        """
        Substitute the lines with all the occurences of <keyword>
        with <updateline> in dftb input file <dftbinput>.
        If outputfile is not specified, the modification is in place
        """
        dftbinput = self._options['inputfile']
        outputfile = dftbinput

        with open(dftbinput, 'r') as inputdata:
            #List constructor makes a hard copy
            lines = list(inputdata.readlines())
            for ind,line in enumerate(lines):
                if keyword.lower() in line.lower():
                    #We need to access directly lines because strings
                    #are immutable, list are not
                    lines[ind] = updateline + '\n'
        
        with open(outputfile, 'w') as outputdata:
            for line in lines:
                outputdata.write(line)

        return 0

    def _read_eq_hs(self):
        """
        Read and store in memory the equilibrium H and S.
        """

        hfile = self._workdir[0] + '/hamsqr1.dat'
        sfile = self._workdir[0] + '/oversqr.dat'
        
        self._eq_h = outputparser.get_hs_sqr(filename = hfile)
        self._eq_s = outputparser.get_hs_sqr(filename = sfile)
