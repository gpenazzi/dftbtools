import numpy as np


def thermal_broadening_function(temperature, energy):
    """Return the value of thermal broadening function  for a given energy
    at a given temperature"""
    kb = np.float(8.6173324e-5)
    kbt = kb * temperature
    #print 'kbt', kbt
    x = energy / (2.0 * kbt)
    f_t = ( (1.0 / (4.0 * kbt) ) * 
    np.power(( 2.0 * np.exp(x) / (np.exp(2.0 * x) + 1.0)), 2) )
    
    return f_t
    

def thermal_average(energies, trans, temperature):
    """Return thermal averaging of transmission. The thermal broadening
    function is assumed to be non-zero on an interval of +-4KbT"""
    
    #We assume that the energies are equally spaced
    delta_e = np.float(energies[1] - energies[0])
    filter_cutoff = (8.6173324e-5 * 4.0 * np.float(temperature))
    #print 'filter_cutoff', filter_cutoff
    filter_size = np.int(filter_cutoff / delta_e) * 2.0 + 1
    #print 'filter_size', filter_size
    f_t = thermal_broadening_function(temperature, 
    np.linspace(-filter_cutoff, +filter_cutoff, filter_size))
    
    t_eff = np.convolve(trans, f_t, mode = 'same') * delta_e
    
    return t_eff

def get_trans_point(energies, trans, energy, temperature = 0.0):
    """Return the transmission in a specific energy point at a given
    temperature. We assume that the input transmission refers to 0K.
    The transmission is interpolated linearly on the energy points."""

    if temperature != 0.0:
        th_trans = thermal_average(energies, trans, temperature)
    else: 
        th_trans = trans

    #Find energy point index
    ind_left = 0
    ind_right = 0
    found = False
    for ind, en in enumerate(energies[1:]):
        if energy > energies[ind-1] and energy <= energies[ind]:
            #Linear interpolation
            t = th_trans[ind-1] + (th_trans[ind] - th_trans[ind-1]) * (energy - \
                    energies[ind-1]) / (energies[ind] - energies[ind-1])
            found = True
            return t
    
    if found == False:
        raise RuntimeError('Could not resolve transmission point')


