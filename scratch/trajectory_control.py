# -*- coding: utf-8 -*-
"""
Created on Sun Apr  8 14:38:33 2012

@author: gpenazzi
"""

import os
import shutil
import fragments
import car
import dftb_utils
from ase.calculators.dftb import Dftb
import sys
import fo_h


class TrajectoryControl():
    """Manage trajectories and operations common to all the snapshots in the 
    trajectory. Drives operations on the single snapshot"""
    
    def __init__(self, traj_path, calc_path, snapshots_prefix, 
                 snapshots_list, frag_to_run = 'all'):
        """Constructor:
            traj_path: directory with trajectories
            calc_path: directory where to setup the calculation
            snapshot_prefix: identifier common to all snapshots
            frag_to_calc: fragments we want to calculate"""
        self._calc_path = calc_path
        self._traj_path = traj_path
        self._snapshots_list = snapshots_list
        self._snapshots_prefix = snapshots_prefix
        self._frag_to_run = frag_to_run        
                
    def run_snapshots(self, task = 'fragments', foh_filename = 'foh_el.dat'):
        """Create snapshot directories, populate them with the right 
        trajectory file"""
        foh_element = list()
        for snapshot in self._snapshots_list:
            print 'Run snapshot ', snapshot
            snapshot_dir = self._calc_path + '/' + str(snapshot)
            if not os.path.exists(snapshot_dir):
                os.mkdir(snapshot_dir)
            #Copy right trajectory file
            filename = self._snapshots_prefix + str(snapshot) + 'ps.car'
            shutil.copyfile(self._traj_path + '/' + filename, 
            snapshot_dir + '/' + filename)
            snapshot_control = SnapshotControl(self._calc_path, snapshot_dir,
            snapshot_dir + '/' + filename,
            fragment_types = ('CNT', 'PC'), frag_to_run = self._frag_to_run)
            
            if task == 'fragments':
                snapshot_control.run_fragment()
            elif task == 'inter_fragments':
                frags = self._frag_to_run
                frag_1 = frags[0]
                frag_2 = frags[1]
                orbitals_1 = range(frags[2][0], frags[2][1])
                orbitals_2 = range(frags[3][0], frags[3][1])
                snapshot_control.initialize_inter_fragment(frag_1, frag_2)
                #I moved fil opening here because doing it at every loop step was a terrible waste of I/O
                with open(foh_filename, 'a') as foh_file:
                    for orb_1 in orbitals_1:
                        for orb_2 in orbitals_2:
                            foh_element = snapshot_control.run_inter_fragment(orb_1, orb_2)
                        
                            foh_file.write('%5i %4s %4i %6i %4s %4i %6i %20.10e \n' % (
                            snapshot, frag_1[0], frag_1[1], orb_1, frag_2[0], frag_2[1], orb_2,
                            foh_element))
                snapshot_control.finalize_inter_fragment()
            elif task == 'copy_cnt1':
                snapshot_control.copy_fragment_cnt1()
            elif task == 'write_onsite_file':
                eigenvalues_dict = snapshot_control.get_onsites()
                filename = 'onsites.dat'
                with open(filename, 'a') as onsite_file:
                    for frag in eigenvalues_dict:
                        for n, eigen in enumerate(eigenvalues_dict[frag]):
                            onsite_file.write('%5i %4s %4i %6i %20.10e \n' % (
                            snapshot, frag[0], frag[1], n + 1, eigen))
            else:
                sys.exit('Wrog task in run_snapshots')
            print 'Done'
    
class SnapshotControl():
    """For every snapshot, perform the calculations requested"""
    
    def __init__(self, root, snapshot_dir, image_file, 
        fragment_types = ('CNT', 'PC'), frag_to_run = 'all'):
        """Constructor.
        calc_path: root directory for the calculation 
        (contains some common informations)"""
        self._root = root
        self._image_file = image_file
        self._fragment_manager = fragments.FragmentManager()
        self._fragment_types = fragment_types
        self._snapshot_dir = snapshot_dir
        #Read image from file and fragment image
        self._snapshot_image = car.read_car(self._image_file)

        #If fragment file is here or in root path, read it
        fragments_file = None
        if os.path.isfile(self._snapshot_dir + '/' + 'fragments.dat'):
            fragments_file = self._snapshot_dir + '/' + 'fragments.dat'
        if os.path.isfile(self._root + '/' + 'fragments.dat'):
            fragments_file = self._root + '/' + 'fragments.dat'
        if fragments_file == None:    
            self._fragment_manager.fragment_image( 
            self._fragment_types, self._snapshot_image)
            self._fragment_manager.save_fragments('fragments.dat')
        else:
            self._fragment_manager.fragment_image( 
            self._fragment_types, self._snapshot_image, read = fragments_file)
        
        if frag_to_run == 'all':
            frag_to_run = self._fragment_manager.get_fragments_ids()
        self._frag_to_run = frag_to_run
        print "frag_to_run is ", self._frag_to_run
            
        

    def run_fragment(self):
        """Run snapshot calculation"""
        self.setup_fragments()
        
    def copy_fragment_cnt1(self):
        """Run snapshot calculation"""
        frag = ('CNT', 1)
        frag_string = frag[0] + str(frag[1])
        frag_dir = self._snapshot_dir + '/' + frag_string
        cnt0_dir = self._snapshot_dir + '/' + 'CNT0'
        if not os.path.exists(frag_dir):
                shutil.copytree(cnt0_dir, frag_dir)

        
    def get_onsites(self, filename = 'on_site.dat'):
        """Return a dictionary with fragment as key and eigenvalues as items"""
        eigenvalues_dict = dict()
        for frag in self._frag_to_run:
            frag_string = frag[0] + str(frag[1])
            frag_dir = self._snapshot_dir + '/' + frag_string
            parser = dftb_utils.DftbOutputParser(frag_dir) 
            eigenvalues = parser.get_eigenvalues(units = 'eV')
            eigenvalues_dict[frag] = eigenvalues
        return eigenvalues_dict


    def setup_fragments(self, qmmm = True):
        """Setup input files and directories for fragment calculation"""
        
        for frag in self._frag_to_run:
            print "Setting fragment ", frag
            frag_string = frag[0] + str(frag[1])
            #Setup directories
            frag_dir = self._snapshot_dir + '/' + frag_string
            if not os.path.exists(frag_dir):
                os.mkdir(frag_dir)
            #This is here because we need a "fake" fragment in our calculation
            #Remove for a generic use
            if frag == ('CNT', 1):
                frag_image = self._fragment_manager.get_fragment_image(
                ('CNT', 0))
                z_v = frag_image.get_cell()[2]
                frag_image.translate(z_v)
            else:
                #this is for every other fragment, only this should be there
                frag_image = self._fragment_manager.get_fragment_image(frag)
                        
            #This is here because we need a "fake" fragment in our calculation
            #Remove for a generic use
            if frag == ('CNT', 1):
                frag_image = self._fragment_manager.get_fragment_image(
                ('CNT', 0))
                z_v = frag_image.get_cell()[2]
                frag_image.translate(z_v)
            
            frag_image.write(frag_dir + '/' + frag_string + '.gen')
            if qmmm:
                filename = self._snapshot_dir + '/' + frag_string + '/' + 'charges.dat'
                if frag == ('CNT', 1):
                    self.setup_qmmm_charge(('CNT', 0), filename)
                else:
                    self.setup_qmmm_charge(frag, filename)
                #shutil.move('charges.dat', self._snapshot_dir + '/' + 
                #frag_string + '/' + 'charges.dat')
                
            os.environ['DFTB_PREFIX'] = '/home/gpenazzi/slako/mio-0-1/'
            if frag[0] == 'CNT':
                os.environ['DFTB_COMMAND'] = '/home/gpenazzi/projects/CNTPoly/PC/bin/dftb+_cnt'
            if frag[0] == 'PC':
                os.environ['DFTB_COMMAND'] = '/home/gpenazzi/projects/CNTPoly/PC/bin/dftb+_pc'
            else:
                os.environ['DFTB_COMMAND'] = '/home/gpenazzi/projects/CNTPoly/PC/bin/dftb+'
            calc = Dftb(label = frag_string, write_dftb = True,
            fermi_temperature = 300.0, scc = True, external_charges = qmmm, 
            write_eigenvectors = True)
            os.chdir(frag_dir)
            print "Calculating fragment ", frag
            calc.initialize(frag_image)
            calc.calculate(frag_image)
            #NOTE: hardcoded for not wasting too much disk space
            #if frag[0] == 'CNT':
            #    homo_index = 1160
            #if frag[0] == 'PC':
            #    homo_index = 360
                
            parser = dftb_utils.DftbOutputParser(frag_dir)
            #parser.reduce_eigenvector_file(homo_index - 10, homo_index + 10)
             #If existing, delete "eigenvector.bin"
            if os.path.isfile('eigenvec.bin'):
                os.remove('eigenvec.bin')
            os.chdir(self._root)
            print "Next"
        

    def setup_qmmm_charge(self, fragment, filename = 'charges.dat'):
        """Setup charges.dat for qm/mm coupling. If one fragment is specified,
        it will setup the file for the single fragment calculation. If 
        a list with two elements is specified, it will setup the file for the 
        interfragment calculation"""
        parser = dftb_utils.DftbOutputParser(self._root)
        f_m = self._fragment_manager
        pos = f_m.get_image().get_positions()
        ch_ind = f_m.get_fragment_index(fragment, mode = 'complement')
        parser.write_charges(pos, filename = filename, ind = ch_ind)
        
    def run_inter_fragment(self, orb_1 = 1160, orb_2 = 1160):
        """Calculate fragment orbital interaction"""
        foh_element = self._ct.do_foh_element(orb_1, orb_2) 
        return foh_element

    def initialize_inter_fragment(self, frag_1 = ('CNT', 0), frag_2 = ('CNT', 1)):
        os.chdir(self._snapshot_dir)
        self._ct = fo_h.FOH(self._fragment_manager)
        self._ct.set_frags(frag_1, frag_2)
        self._ct.set_input()
        self._ct.run_dftb(pbc = False)
        self._ct.get_hs()
        return 0

    def finalize_inter_fragment(self):
        self._ct.release()
        os.chdir('../')
        return 0


