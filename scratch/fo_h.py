# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 21:12:51 2012
This module contains the routines needed to calculate Fragment Orbital 
Hamiltonian elements 
@author: gpenazzi
"""

import os
import ase
import dftb_utils
import fragments
import math
import shutil

def string_to_frag_id(name):
    """Convert string id to t-upla id"""
    string_to_frag = {"CNT0" : ("CNT", 0), "CNT1" : ("CNT", 1), 
                      "PC0" : ("PC", 0), 
               "PC1" : ("PC", 1), "PC2" : ("PC", 2),
               "PC3" : ("PC", 3), "PC4" : ("PC", 4), "PC5" : ("PC", 5),
                "PC6" : ("PC", 6), "PC7" : ("PC", 7), "PC8" : ("PC", 8),
                "PC9" : ("PC", 9), "PC10" : ("PC", 10), "PC11" : ("PC", 11),
                "PC12" : ("PC", 12), "PC13" : ("PC", 13), "PC14" : ("PC", 14),
                "PC15" : ("PC", 15), "PE0": ("PE", 0), "PE1": ("PE", 1),
                "PE2": ("PE", 2), "PE3": ("PE", 3),"PE4": ("PE", 4),
                 "PE5": ("PE", 5), "PE6": ("PE", 6),"PE7": ("PE", 7),
                "PE8": ("PE", 8), "PE9": ("PE", 9), "PE10": ("PE", 10),
                 "PE11": ("PE", 11),  "PE12": ("PE", 12), "PE13": ("PE", 13),
                 "PE14": ("PE", 14), "PC16" : ("PC", 16), "PC17" : ("PC", 17),
                 "PC18" : ("PC", 18), "PC19" : ("PC", 19)}
    return string_to_frag[name]
                
def frag_id_to_string(frag_id):
    """Convert t-upla id to string id"""
    frag_to_string = {("CNT", 0): "CNT0", ("CNT", 1): "CNT1", 
                      ("PC", 0): "PC0", 
                ("PC", 1): "PC1", ("PC", 2):  "PC2",
                ("PC", 3): "PC3", ("PC", 4):  "PC4", ("PC", 5):  "PC5",
                ("PC", 6): "PC6",("PC", 7):  "PC7", ("PC", 8):  "PC8",
                ("PC", 9): "PC9",("PC", 10):  "PC10",("PC", 11):  "PC11",
                ("PC", 12): "PC12", ("PC", 13): "PC13", ("PC", 14): "PC14",
                ("PC", 15): "PC15", ("PE", 0): "PE0", ("PE", 1): "PE1",
                 ("PE", 2): "PE2",("PE", 3): "PE3",  ("PE", 4): "PE4", 
                 ("PE", 5): "PE5",("PE", 6): "PE6", ("PE", 7): "PE7" ,
                ("PE", 8): "PE8", ("PE", 9): "PE9",  ("PE", 10): "PE10",
                ("PE", 11): "PE11", ("PE", 12): "PE12", ("PE", 13): "PE13",
                ("PE", 14): "PE14", ("PC", 16): "PC16", ("PC", 17): "PC17",
                ("PC", 18): "PC18", ("PC", 19): "PC19" }
    return frag_to_string[frag_id]


class FOH:
    """Calculate FO Hamiltonian elements between different fragments"""

    def __init__(self, fragment_manager):
        """Constructor"""
        self._frag_1 = ("NULL", 0)
        self._frag_2 = ("NULL", 0)
        #Numbers of atoms in fragments. Should be in Fragment when the two
        #classes will communicate             
        self._frag_1_n = 0
        self._frag_2_n = 0
        self._fragment_manager = fragment_manager

        self._frag_1_dir = None
        self._frag_2_dir = None
        self._working_dir = None
        self._frag_1_image = None
        self._frag_2_image = None
        self._interaction_h = None
        self.interaction_overlap = None
        self._interactions_1_2 = None
        
        self._eigenvectors = dict()
        self._eigenvalues = dict()
        
    def set_frags(self, frag_1, frag_2):
        """Set fragment couple"""
        self._frag_1 = frag_1
        self._frag_1_dir = frag_id_to_string(self._frag_1) + "/"
        self._frag_2 = frag_2
        self._frag_2_dir = frag_id_to_string(self._frag_2) + "/"
        self._working_dir = (frag_id_to_string(self._frag_1) + 
        frag_id_to_string(self._frag_2) + "/")
        
    
#    def _create_gen_file(self):
#        """Read images from fragment calculation directories.
#        Create structure for the input file (merge two fragments)"""
#        self._create_image().write(
#        self._working_dir + frag_id_to_string(self._frag_1) +
#        frag_id_to_string(self._frag_2) + ".gen")
                
#    def _create_image(self, pbc = True):
#        """Read images from fragment calculation directories.
#        Create structure for the input file (merge two fragments)"""
#        frag_1_image = ase.io.read(self._frag_1_dir + 
#        frag_id_to_string(self._frag_1) + ".gen")
#        frag_2_image = ase.io.read(self._frag_2_dir + 
#        frag_id_to_string(self._frag_2) + ".gen")
#        self._frag_1_n = frag_1_image.get_number_of_atoms()
#        self._frag_2_n = frag_2_image.get_number_of_atoms()
#        
#        structure = ase.atoms.Atoms()
#        if pbc:
#            structure.set_pbc(frag_1_image.get_pbc())
#            structure.set_cell(frag_1_image.get_cell())
#        if not pbc: 
#            structure.set_pbc((False, False, False))
#        structure += frag_1_image
#        structure += frag_2_image
#        return structure

    def _create_image(self, pbc = True):
        """Create imge with two fragments"""
        frag_1_image = self._fragment_manager.get_fragment_image(self._frag_1)
        frag_2_image = self._fragment_manager.get_fragment_image(self._frag_2)
        self._frag_1_n = frag_1_image.get_number_of_atoms()
        self._frag_2_n = frag_2_image.get_number_of_atoms()
        
        structure = ase.atoms.Atoms()
        if pbc:
            structure.set_pbc(frag_1_image.get_pbc())
            structure.set_cell(frag_1_image.get_cell())
        if not pbc: 
            structure.set_pbc((False, False, False))
        structure += frag_1_image
        structure += frag_2_image
        return structure
        
        
    def set_input(self):
        """Set the input file where to place the fo hopping matrix element
        calculation"""
        #Create working directory
        if not os.path.exists(self._working_dir):
            os.makedirs(self._working_dir)
            
    def run_dftb(self, pbc = True):
        #Move to the working directory and invoke the calculation using ase
        structure = self._create_image(pbc)
        os.environ['DFTB_PREFIX'] = '/home/gpenazzi/slako/unofficial/mine-16-7/'
#        os.environ['DFTB_PREFIX'] = '/home/gpenazzi/slako/unofficial/mio-8-7/'

        os.environ['DFTB_COMMAND'] = '/home/gpenazzi/projects/CNTPoly/PC/bin/dftb+'
        os.chdir(self._working_dir)
        calc = ase.calculators.dftb.Dftb(label=frag_id_to_string(self._frag_1)
            + frag_id_to_string(self._frag_2),
            write_dftb=True,
            do_spin_polarized=False,
            fermi_temperature=300.0,
            scc=False, write_real_hs = True)
        #print "I'm going to run dftb"
        calc.initialize(structure)
        calc.calculate(structure)
        structure.write('input.gen')
        #print "Done"
        os.chdir("../")

    def get_hs(self):
        """Get H and S from file"""
        #print "I'm trying to get the Hamiltonian"
        parser = dftb_utils.DftbOutputParser(self._working_dir)
        self._interaction_h = parser.get_hs_real(task = 'h')
        self._interaction_s = parser.get_hs_real(task = 's')
        #print "It's done"
        
    def get_fragments_interactions(self):
        """Returns a list [atm1_index, orb1, atm2_index, orb2] with the interactions
        interfragmnt only"""
        
        interactions_1_2 = list()
        for key in self._interaction_h.keys():
            if key[0] < self._frag_1_n and key[2] > self._frag_1_n:
                interactions_1_2.append([key[0], key[1], key[2], key[3]])
        self._interactions_1_2 = interactions_1_2
        return interactions_1_2
        
    def do_foh_element(self, i, j):
        """Calculate interaction between orbital i of fragment 1 and 
        orbital j of fragment 2"""
        eigenvec_1 = self.get_eigenvector((self._frag_1, i))
        eigenval_1 = self.get_eigenvalue((self._frag_1, i))
        #parser.reduce_eigenvector_file(i - 20, i + 20)
        eigenvec_2 = self.get_eigenvector((self._frag_2, j))
        eigenval_2 = self.get_eigenvalue((self._frag_2, j))
        #parser.reduce_eigenvector_file(j - 20, j + 20)
        
        foh_element = 0.0
        s_element = 0.0
        foh_ort = 0.0
        if self._interactions_1_2 == None:
            interactions = self.get_fragments_interactions()
        else:
            interactions = self._interactions_1_2
        for inter in interactions:
            atm1 = inter[0]
            orb1 = inter[1]
            atm2 = inter[2]
            orb2 = inter[3]
            tmp_h = (eigenvec_1[atm1][orb1] *
            eigenvec_2[atm2 - self._frag_1_n][orb2] *
            self._interaction_h[(atm1, orb1, atm2, orb2)]) * 27.2116
            tmp_s = (eigenvec_1[atm1][orb1] *
            eigenvec_2[atm2 - self._frag_1_n][orb2] *
            self._interaction_s[(atm1, orb1, atm2, orb2)])
            foh_element = foh_element + tmp_h
            s_element = s_element + tmp_s
        #print "Element before orthogonalizations is ", foh_element
        #Orthogonalization
        foh_ort = foh_element - s_element * (eigenval_1 + eigenval_2) / 2.0
        #print "Element after orthogonalizations is ", foh_ort
        #filename = self._working_dir + '/' + 'foh_element.dat'
        #with open(filename, 'w') as outputfile:
        #    out_string = 'orb1 ' + str(i) + ' orb2' + str(j) +  ' = ' + str(foh_ort) + '\n'
        #    outputfile.write(out_string)
        return foh_ort
        
    def get_eigenvector(self, frag_orb):
        """To improve speed we store eigenvectors, as they likely will be 
        reused and we check if we must read it from file or if we already 
        have it"""
        if frag_orb in self._eigenvectors:
            return self._eigenvectors[frag_orb]
        else:
            if frag_orb[0] == self._frag_1:
                parser = dftb_utils.DftbOutputParser(self._frag_1_dir)
            if frag_orb[0] == self._frag_2:
                parser = dftb_utils.DftbOutputParser(self._frag_2_dir)
            eigenvec = parser.get_eigenvector(frag_orb[1])
            self._eigenvectors[frag_orb] = eigenvec
            return eigenvec
            
    def get_eigenvalue(self, frag_orb):
        """To improve speed we store eigenvectors, as they likely will be 
        reused and we check if we must read it from file or if we already 
        have it"""
        if frag_orb in self._eigenvalues:
            return self._eigenvalues[frag_orb]
        else:
            if frag_orb[0] == self._frag_1:
                parser = dftb_utils.DftbOutputParser(self._frag_1_dir)
            if frag_orb[0] == self._frag_2:
                parser = dftb_utils.DftbOutputParser(self._frag_2_dir)
            eigenvalue = parser.get_eigenvalues()[frag_orb[1]]
            self._eigenvalues[frag_orb] = eigenvalue
            return eigenvalue
        
        
    def release(self):
        """Release operation for fo calculator. Delete working directory 
        and related content to avoid memory blowout"""
        shutil.rmtree(self._working_dir)
