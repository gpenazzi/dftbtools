"""This module contains methods for partitioning an atomistic structure
   into fragments """

from ase import Atoms
from ase.calculators.neighborlist import NeighborList
import sys

class FragmentManager:
    """Manage structure import, fragments creations, fragment
    manager etc. """
    
    def __init__(self):
        self.image = Atoms()
        self.fragments = list()
        self.fragment_dictionary = {"NULL":0}
        self.nl = []
        self._fragment_instances = []
        
    def save_fragments(self, filename):
        """Save fragment list to file"""   
        with open(filename, 'w') as fileobject:
            for frag in self.fragments:
                file_string = str(frag[0]) + '   ' + str(frag[1]) + '\n'
                fileobject.write(file_string)
                
    def load_fragments(self, filename):
        """Read fragments from file"""
        with open(filename, 'r') as fileobject:
            lines = fileobject.readlines()
            for ind, line in enumerate(lines):
                if len(line.split()) > 1:
                    self.fragments[ind] = ((line.split()[0], 
                    int(line.split()[1])))
                                           
    def get_image(self):
        """Get overall image"""
        return self.image
        
    def get_fragment_instance(self, fragment_id):
        """Get a reference to fragment instance identified by id"""
        for inst in self._fragment_instances:
            if inst.get_fragment_id() == fragment_id:
                return inst
        return 0
        
    def get_fragments_ids(self):
        """Get list of fragment ids"""
        fragment_ids = []
        for frag in self._fragment_instances:
            fragment_ids.append(frag.get_fragment_id())
        return fragment_ids
        
    def get_fragment_image(self, fragment_id, preserve_pbc = True, 
                           mode = 'normal'):
        """Get fragment image. If mode = 'complement' is specified, it returns
        an image containing all the atoms except the fragment ones."""
        #This ('CNT', 1)stuff is a dirty hackfo my calculations . 
        #To be removed in "general" versions
        dirty_hack = False
        if fragment_id == ('CNT', 1):
            dirty_hack = True
            fragment_id = ('CNT', 0)
        
        fragment_image = Atoms()
        compl_image = Atoms()
        if preserve_pbc:
            fragment_image.set_pbc(self.image.get_pbc())
            fragment_image.set_cell(self.image.get_cell())
            compl_image.set_pbc(self.image.get_pbc())
            compl_image.set_cell(self.image.get_cell())
        for ind, atm in enumerate(self.image):
            if self.fragments[ind] == fragment_id:
                fragment_image.append(atm)
            else:
                compl_image.append(atm)
        if fragment_image.get_number_of_atoms() == 0:
            sys.exit('Error: returned empty fragment image')
        if mode == 'normal':
            if dirty_hack:
                z_v = fragment_image.get_cell()[2]
                fragment_image.translate(z_v)
                return fragment_image
            return fragment_image
        if mode == 'complement':
            if dirty_hack:
                z_v = compl_image.get_cell()[2]
                compl_image.translate(z_v)
                return compl_image
            return compl_image
    
    def set_structure(self, image):
        """Set the atomistic structure you want to divide in fragments"""
        if isinstance(image, Atoms):
            self.image = image
        if isinstance(image, list):
            self.image = image[0]
       

#        for ind, atm in enumerate(image):
#            if len(self.nl.get_neighbors(ind)[0]) > 3:
#                print "ATOM ",ind, " has ", self.nl.get_neighbors(ind)[0], " neighbors"
        
        #Initialize self.fragments to the right number of elements
        for i in range(self.image.get_number_of_atoms()):
            self.fragments.append(("NULL", 0))
    
    def create_fragment(self, fragment_type):
        """Create a new fragment"""
        #TODO: exceptions for this kind of stuff
        if fragment_type in self.fragment_dictionary:
            self.fragment_dictionary[fragment_type] = (
            self.fragment_dictionary[fragment_type] + 1)
        else:
            self.fragment_dictionary[fragment_type] = 0
        fragment_id = ((fragment_type, 
                      self.fragment_dictionary[fragment_type]))
                          
        if (fragment_type == 'CNT'):
            print 'Create CNT'
            fragment = FragmentCnt(self, fragment_id)
            
        if (fragment_type == 'PC'):
            print 'Create PC'
            fragment = FragmentPc(self, fragment_id)     
            
        if (fragment_type == 'PE'):
            print 'Create PE'
            fragment = FragmentPe(self, fragment_id)
            
        return fragment
            
    def _set_cutoff_list(self):
        """Set cutoff list for a correct assembly of neighbors map"""
         #Set neighbor list
        c_cutoff = 0.75
        h_cutoff = 0.5
        o_cutoff = 0.75

        #Set up cutoff radii list
        radii = self.image.get_number_of_atoms()*[0.0]
        i = 0
        for atm in self.image.get_chemical_symbols():
            if (atm == "C"): 
                radii[i] = c_cutoff
            elif (atm == "H"): 
                radii[i] = h_cutoff
            elif (atm == "O"): 
                radii[i] = o_cutoff
            else:
                print "Error: is not C or H or O"
            i += 1
        print "Cutoff radii list ready"
        return radii
     
    def fragment_image(self, fragment_types, image, read = None):
        """Split the image in fragments defined by fragment types.
        As far as it's possible to define a fragment, it will be 
        built. The function will stop when no more fragments can 
        be defined.
        if read = path is specified, it will read fragments from file"""
        self.set_structure(image)
        
        if read == None:
            #Create fragments for each fragment_types entry until 
            #no more fragments are created        
            #Neighbor list is needed for this operation
            self.nl = NeighborList(self._set_cutoff_list())
            self.nl.bothways = True
            self.nl.self_interaction = False
            self.nl.build(self.image)
            print "Neighbor list built"
            
            for fragment_type in fragment_types:
                seed_exists = True
                while seed_exists:
                    self._fragment_instances.append(
                    self.create_fragment(fragment_type))
                    tmp = self._fragment_instances[-1].find_seed()
                    if not(tmp == -1):
                        self._fragment_instances[-1].scc_process_atoms()
                    else:
                        self._fragment_instances.pop(-1)
                        seed_exists = False
        else:
            self.load_fragments(read)
                    
        for ind, frag in enumerate(self.fragments):
            if frag == ('NULL', 0):
                err = 'Error: atom ' + str(ind) + ' is not in a fragment'
                sys.exit(err)                    
                    
                if (
                self._fragment_instances[-1].get_image().get_number_of_atoms()
                > 1000):
                    sys.exit('Warning: a fragment has more than 1000 atoms.' +
                    'If you think it\'s ok, modify check at fragments.py::120')
        
    def write_to_file(self, file_type):
        """Write all fragments to file. Format is specified by
        file_type (extensions supported by ASE)"""
        for fragment in self._fragment_instances:
            fragment.write_to_file(file_type)
            
    def get_fragment_index(self, fragment, mode = 'normal'):
        """Get the indexes defining the fragment from the
        starting image. If mode = 'complement' is defined, 
        get all the indexes expect the fragment ones"""
        
        #This ('CNT', 1)stuff is a dirty hackfo my calculations . 
        #To be removed in "general" versions
        if fragment == ('CNT', 1):
            fragment = ('CNT', 0)
        
        ind_normal = list()
        ind_comp = list()
        for ind, frag in enumerate(self.fragments):
            if frag == fragment:
                ind_normal.append(ind)
            else:
                ind_comp.append(ind)                
        if mode == 'normal':
            return ind_normal
        elif mode == 'complement':
            return ind_comp
        return []

class Fragment:
    """Base class for managing fragments"""
    
    def __init__(self, fragment_manager, fragment_id):
        self._fm = fragment_manager
        self._seed = 0
        self._nl = fragment_manager.nl
        self._image = self._fm.image
        self._fragments = fragment_manager.fragments
        self._fragment_id = fragment_id
        
    def get_image(self):
        """Get fragment image"""
        return self._image
        
    def _is_seed(self, atm_ind):
        """Fake virtual method"""
        sys.exit("Error:_is_seed() is not implemented in base class Fragment")
    
    def get_rules(self):
        """Fake virtual method"""
        sys.exit("Error:get_rules() is not implemented in base class Fragment")        
        
    def get_fragment_id(self):
        """Return id of the fragment"""
        return self._fragment_id
    
    def find_seed(self):
        """Find the seed atom"""
        #Seed atom must not belong alredy to a fragment,
        #and it must satisfy the is_seed condition
        for i in range(self._image.get_number_of_atoms()):
            if (self._fragments[i] == ("NULL", 0) and
                self._is_seed(i)):
                self._fragments[i] = self._fragment_id
                return i
        
        return -1
        
    #Different rules define if an atom belongs to the given fragment    
    def _rule_0(self, atm_index):
        """Rules are defined assuming that a seed is present.
           Rule number 0: an atom can be assigned to a fragment if it's 
           not already. This rule is common to all the fragments"""
        if self._fragments[atm_index] == ("NULL", 0):
            return True
        else:
            return False
            
    def process_atoms(self):
        """Process all atoms according to the rules. This 
           call drives a single sweep. For the full result, run
           scc_process_atoms"""
        at_least_one = False
        for atm_index in range(self._image.get_number_of_atoms()):
            belongs_to_fragment = True
            for rule in self.get_rules():
                if not rule(atm_index):
                    belongs_to_fragment = False
            if belongs_to_fragment:
                self._fragments[atm_index] = self._fragment_id
                at_least_one = True
        return at_least_one
        
    def scc_process_atoms(self):
        """Process atoms iteratively until no atom is assigned
           to the fragment (fragment is complete)"""
        at_least_one = True
        while at_least_one:
            at_least_one = self.process_atoms()
            
    def append_fragment_to_image(self, image):
        """Copy fragment in its own image"""
        for atm_index in range(self._image.get_number_of_atoms()):
            if self._fragments[atm_index] == self._fragment_id:
                image.append(self._image[atm_index])
        #Fragment has the same periodicity of the belonging image
        image.set_pbc(self._image.get_pbc())
        image.set_cell(self._image.get_cell())
                
    def write_to_file(self, file_type):
        """Write fragment to file. Format is specified by
        file_type (extensions supported by ASE)"""
        
        name = (self._fragment_id[0] + str(self._fragment_id[1]) + 
        "." + file_type)
        tmp_image = Atoms()
        self.append_fragment_to_image(tmp_image)
        tmp_image.write(name)
            

class FragmentCnt(Fragment):
    """CNT fragment class"""
    
    def __init__(self, fragment_manager, fragment_id):
        Fragment.__init__(self, fragment_manager, fragment_id)
        
        
    def _is_seed(self, atm_ind):
        """Set of rules to define if an atom (labeled by
        index) is a seed""" 
        atm = self._image[atm_ind]
        #Atom is carbon?
        if atm.symbol=="C":
            conditions = [True]
        else: 
            return False
        
        nn = self._nl.get_neighbors(atm_ind)[0] 
        
        #Atom has 3 neighbors?
        if len(nn)==3:
            conditions.append(True)
        else:
            return False
            
        #Neighbors are carbons?
        if (self._image.get_chemical_symbols()[nn[0]]=="C" and 
            self._image.get_chemical_symbols()[nn[1]]=="C" and 
            self._image.get_chemical_symbols()[nn[2]]=="C"):
            conditions.append(True)
        else:
            return False
            
        #One neighbor (first) has 3 neigbors?
        neigh_ind = nn[0]
        nn_neigh = self._nl.get_neighbors(neigh_ind)[0] 
        if len(nn_neigh)==3:
            conditions.append(True)
        else:
            return False
            
        #Neighbors of neighbor are carbons?
        if (self._image.get_chemical_symbols()[nn_neigh[0]]=="C" and 
            self._image.get_chemical_symbols()[nn_neigh[1]]=="C" and 
            self._image.get_chemical_symbols()[nn_neigh[2]]=="C"):
            conditions.append(True)
        else:
            return False
            
        #The conditions defined up to now permit to identify the
        #seed of a CNT with no ambiguity in a PC or PE environment.
        #It could be different in your system. Check it out
        for cond in conditions:
            if not cond:
                return False
                
        return True
        
    #Different rules define if an atom belongs to the given fragment    
    def _rule_1(self, atm_index):
        """Rules are defined assuming that a seed is present.
           Rule number 1: an atom can be assigned to a CNT fragment if it's 
           bonded to an atom which belongs to the fragment"""
        nn = self._nl.get_neighbors(atm_index)[0]
        for nn_ind in nn:
            if self._fragments[nn_ind] == self._fragment_id:
                return True
        return False
        
    def get_rules(self):
        """Return a list of all belongs-to-fragment rules"""
        return [self._rule_0, self._rule_1]
    

class FragmentPc(Fragment):
    """Recognize and manage PC full chain fragments"""
    
    def __init__(self, fragment_manager, fragment_id):
        Fragment.__init__(self, fragment_manager, fragment_id)

    def _is_seed(self, atm_ind):
        """Set of rules to define if an atom (labeled by
        index) is a seed""" 
        atm = self._image[atm_ind]
        #Atom is carbon?
        if atm.symbol=="O":
            return True
        else: 
            return False
            
    #Different rules define if an atom belongs to the given fragment    
    def _rule_1(self, atm_index):
        """Rules are defined assuming that a seed is present.
           Rule number 1: an atom can be assigned to a CNT fragment if it's 
           bonded to an atom which belongs to the fragment"""
        nn = self._nl.get_neighbors(atm_index)[0]
        for nn_ind in nn:
            if self._fragments[nn_ind] == self._fragment_id:
                return True
        return False
        
    def get_rules(self):
        """Return a list of all belongs-to-fragment rules"""
        return [self._rule_0, self._rule_1]
    

class FragmentPe(Fragment):
    """Recognize and manage PE full chain fragments"""
    
    def __init__(self, fragment_manager, fragment_id):
        Fragment.__init__(self, fragment_manager, fragment_id)

    def _is_seed(self, atm_ind):
        """Set of rules to define if an atom (labeled by
        index) is a seed""" 
        atm = self._image[atm_ind]
        #Atom is carbon?i
        if atm.symbol=="H":
            return True
        else: 
            return False
            
    #Different rules define if an atom belongs to the given fragment    
    def _rule_1(self, atm_index):
        """Rules are defined assuming that a seed is present.
           Rule number 1: an atom can be assigned to a CNT fragment if it's 
           bonded to an atom which belongs to the fragment"""
        nn = self._nl.get_neighbors(atm_index)[0]
        for nn_ind in nn:
            if self._fragments[nn_ind] == self._fragment_id:
                return True
        return False
        
    def get_rules(self):
        """Return a list of all belongs-to-fragment rules"""
        return [self._rule_0, self._rule_1]
    
