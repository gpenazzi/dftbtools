# -*- coding: utf-8 -*-
"""
Created on Thu Aug 16 15:39:45 2012

@author: gpenazzi
"""

import numpy as np
import dftbtools
from dftbtools.scratch import green

class FoLandauer():
    """This class set up the Hamiltonian, add wide band self energies 
    and calculate the transmission at a given energy point"""
    
    
    def __init__(self, calc_path, configuration, c1, c2, 
                 self_energy, hopping_filename = 'foh_el.dat', onsite_filename
                 = 'onsites.dat', snapshot = 0):
        """Constructor:
            calc_path: root directory for the calculation
            configuration: hamiltonian configuration as list of (frag, orbital)
                objects. Ex. [(('CNT',0), 1160), (('CNT',1), 1160)] for a 
                two orbital system from CNT0 homo to CNT1 homo
            c1: number of orbitals fixed as a first contact (c1 is always 
            the first block of the hamiltonian)
            c2: number of orbitals fixed as a first contact (c2 is always 
            the last block of the hamiltonian)
            energy: return T(energy)"""
            
        self._calc_path = calc_path
        self._configuration = configuration
        self._c1 = c1 
        self._c2 = c2
        self._self_energy = self_energy
        self._hopping_filename = hopping_filename
        self._onsite_filename = onsite_filename
        self._snapshot = snapshot
        self._onsite_dict = None
        self._hopping_dict = None
        
    def onsite_dictionary(self):
        """Build a dictionary [(frag, orbital)] = onsite """
        filename = self._calc_path + '/' + self._onsite_filename
        onsite_dict = dict()
        with open(filename, 'r') as onsite_file:
            for line in onsite_file:
                words = line.split()
                if int(words[0]) == self._snapshot:
                    onsite_dict[((words[1], int(words[2])), 
                                 int(words[3]))] = float(words[4])
                    
        return onsite_dict
        
        
    def hopping_dictionary(self):
        """Build a dictionary [(frag1, orbital1, frag2, orbital2)] = hopping"""
        filename = self._calc_path + '/' + self._hopping_filename
        hopping_dict = dict()
        with open(filename, 'r') as hopping_file:
            for line in hopping_file:
                words = line.split()
                if int(words[0]) == self._snapshot:

                    frag_orb_1 = ((words[1], int(words[2])), int(words[3]))
                    frag_orb_2 = ((words[4], int(words[5])), int(words[6]))

                    hopping_dict[(frag_orb_1, frag_orb_2)] = float(words[7])                
                    hopping_dict[(frag_orb_2, frag_orb_1)] = float(words[7])

                                
        return hopping_dict
        
        
    def build_hamiltonian(self):
        """Build the FO hamiltonian"""
        n = len(self._configuration)
        hamil = np.matrix(np.zeros((n, n), dtype = np.complex))
        #Cycle on configuration and assign values accordingly
        
        #Onsites
        for i, frag_orb in enumerate(self._configuration):
            hamil[i,i] = self._onsite_dict[frag_orb]
            
        #Hopping
        configuration_2 = list(self._configuration)
        for i, frag_orb_1 in enumerate(self._configuration):
            for j, frag_orb_2 in enumerate(configuration_2):
                #We drop not only diagonal elements, but also intra-fragment
                #hopping elements as there's not any (they are always 0.0)
                if frag_orb_1[0] != frag_orb_2[0]:
                    if (frag_orb_1, frag_orb_2) in self._hopping_dict:
                        hamil[i,j] = self._hopping_dict[(frag_orb_1, frag_orb_2)]
                    elif (frag_orb_2, frag_orb_1) in self._hopping_dict:
                        hamil[i,j] = self._hopping_dict[(frag_orb_2, frag_orb_1)]
                    else:
                        hamil[i,j] = 0.0

        return hamil
        
        
    def build_self_energy(self, n = 1):
        """Build and return self energy relative to contact n (n = 1, 2)"""
        size = len(self._configuration)
        self_energy = np.matrix(np.zeros((size, size), dtype = np.complex))
        if n == 1:
            for i in range(self._c1):
                self_energy[i,i] = - self._self_energy * 1j

        if n == 2:
            for i in range(self._c2):
                self_energy[- (i + 1), - (i + 1)] = - self._self_energy * 1j

            
        return self_energy
        
        
    def calculate_transmission(self, energy):
        """Do all the job and return the transmission"""
        
        self._onsite_dict = self.onsite_dictionary()
        self._hopping_dict = self.hopping_dictionary()
        
        hamil = self.build_hamiltonian()

        self_1 = self.build_self_energy(n = 1)
        self_2 = self.build_self_energy(n = 2)
#        print 'en', self._energy
#        print 'hamil', hamil
#        print 's1', self_1
#        print 's2', self_2
        trans = green.transmission(energy, hamil, self_1, self_2)
        #print 'Transmission at snapshot ', self._snapshot, ' is ', trans
        return trans
        

        
class FoLandauerDriver():
    """Instance FoLandauer class for every snapshot in list and plot 
    resulting transmission"""
    
    def __init__(self, calc_path, configuration, c1, c2, 
                 self_energy, hopping_filename = 'foh_el.dat', onsite_filename
                 = 'onsites.dat', snapshots = [0], energy = [0.0],
                 prefix = './'):
        """Constructor:
            calc_path: root directory for the calculation
            configuration: hamiltonian configuration as list of (frag, orbital)
                objects. Ex. [(('CNT',0), 1160), (('CNT',1), 1160)] for a 
                two orbital system from CNT0 homo to CNT1 homo
            c1: number of orbitals fixed as a first contact (c1 is always 
            the first block of the hamiltonian)
            c2: number of orbitals fixed as a first contact (c2 is always 
            the last block of the hamiltonian)
            energy: return T(energy)"""
        print("Starting FoLandauerDriver")
        self._fo_landauer_snapshots = list()            
        self._transmission = list()
        nonavg = list()
        pos = len(energy)/2
        for snapshot in snapshots:
            fo_instance = FoLandauer(calc_path, configuration, c1, c2, 
                 self_energy, hopping_filename, onsite_filename, snapshot)
            trans = fo_instance.calculate_transmission(energy)
            filename = prefix + '/trans-' + str(snapshot) + '.dat'
            with open(filename, 'w') as t_file:
                for i in range(len((trans))):
                        t_file.write('%5i %20.10e \n' % (energy[i],
                         trans[i]))
            trans_avg = green.thermal_average(energy, trans, 300.0)
            filename = prefix + '/trans-avg-' + str(snapshot) + '.dat' 
            with open(filename, 'w') as t_file:
                for i in range(len((trans_avg))):
                        t_file.write('%20.10e %20.10e \n' % (energy[i],
                         trans_avg[i]))
            self._transmission.append(trans_avg[pos])
            nonavg.append(trans[pos])

        filename = prefix + '/trans-ef-avg.dat'    
        with open(filename, 'w') as t_file:
            for i in range(len((self._transmission))):
                t_file.write('%20.10e %20.10e \n' % (i, self._transmission[i]))
        filename = prefix + '/stats-ef-avg.dat'
        with open(filename, 'w') as t_file:
            t_file.write('%s %20.10e \n' % ('max', np.max(self._transmission)))
            t_file.write('%s %20.10e \n' % ('min', np.min(self._transmission)))
            t_file.write('%s %20.10e \n' % ('avg', np.mean(self._transmission)))

        filename = prefix + '/trans-ef.dat'    
        with open(filename, 'w') as t_file:
            for i in range(len((self._transmission))):
                t_file.write('%20.10e %20.10e \n' % (i, nonavg[i]))
        filename = prefix + '/stats-ef.dat'
        with open(filename, 'w') as t_file:
            t_file.write('%s %20.10e \n' % ('max', np.max(nonavg)))
            t_file.write('%s %20.10e \n' % ('min', np.min(nonavg)))
            t_file.write('%s %20.10e \n' % ('avg', np.mean(nonavg)))

