# -*- coding: utf-8 -*-
"""
Created on Fri Aug 10 10:11:09 2012

@author: gpenazzi
"""

import numpy as np

def transmission(energy, ham, self_energy_1, self_energy_2):
    """ Calculate the transmission give the energy, the hamiltonian and
    contact 1 and 2  self energy. Orthogonal basis and 2 contacts 
    system are assumed"""
    
    
    gamma1= 2.0 * np.imag(self_energy_1)
    gamma2= 2.0 * np.imag(self_energy_2)
    transmission = np.zeros(len(energy))

    for ind, en in enumerate(energy):

        energy_matrix = np.matrix(np.eye(len(ham))) * en
        g = np.linalg.inv(energy_matrix - ham - self_energy_1 - self_energy_2)

    #Note: I take the real part because there can be some imaginary
    #numerical garbage there
        transmission[ind] = np.real(np.trace(gamma1 * g * gamma2 * g.H))


    return transmission
          

def thermal_broadening_function(temperature, energy):
    """Return the value of thermal broadening function  for a given energy
    at a given temperature"""
    kb = np.float(8.6173324e-5)
    kbt = kb * temperature
    #print 'kbt', kbt
    x = energy / (2.0 * kbt)
    f_t = ( (1.0 / (4.0 * kbt) ) * 
    np.power(( 2.0 * np.exp(x) / (np.exp(2.0 * x) + 1.0)), 2) )
    
    return f_t
    

def thermal_average(energies, trans, temperature):
    """Return thermal averaging of transmission. The thermal broadening
    function is assumed to be non-zero on an interval of +-4KbT"""
    
    #We assume that the energies are equally spaced
    delta_e = np.float(energies[1] - energies[0])
    filter_cutoff = (8.6173324e-5 * 4.0 * np.float(temperature))
    #print 'filter_cutoff', filter_cutoff
    filter_size = np.int(filter_cutoff / delta_e) * 2.0 + 1
    #print 'filter_size', filter_size
    f_t = thermal_broadening_function(temperature, 
    np.linspace(-filter_cutoff, +filter_cutoff, filter_size))
    
    t_eff = np.convolve(trans, f_t, mode = 'same') * delta_e
    
    return t_eff

