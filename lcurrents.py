"""
Contains utilities to process and draw local currents from DFTB+ calculations
"""

import numpy as np

class LocalCurrentsTB(object):
    """
    Parse and plot local currents in TB representation
    """
    def __init__(self, filename = 'lcurrents.dat'):
        """
        filename (in) : file containing local currents information
        """
        self.filename = filename

        # Note: lcurrents will be a 3-ple containing 3 lists:
        # neighs(i) = np.array(j1, j2, j3, ...)  all the neighbor indexes for each atom
        # currents(i) = np.array(Iij1, Iij2, Iij3, ...) all the local currents for each atom
        # positions(i) = np.array(x,y,z) all the atom positions
        self.lcurrents = None

        #Maximum local current, useful for renormalization
        self.maxval = None

        self.n_atoms = None
        self.bond_currents = None
        self.atom_currents = None

    def parse_currents(self):
        """
        Read the local current file and fill up a proper
        """
        with open(self.filename, 'r') as lcfile:
            lclines = lcfile.readlines()
            positions = []
            neighs = []
            currents = []
            for line in lclines:
                line_entries = line.split()
                ind = int(line_entries[0])
                positions.append(np.array([float(entry) for entry in line_entries[1:4]]))
                if int(line_entries[4]) == 0:
                    continue
                neigh = np.array([int(entry) - 1 for entry in line_entries[5::2]])
                current = np.array([float(entry) for entry in line_entries[6::2]])
                assert(len(current) == len(neigh))
                neighs.append(neigh)
                currents.append(current)
        n_atoms = len(positions)
        maxval = 0.0
        for atom in range(n_atoms):
            i_max = currents[atom].max()
            if i_max > maxval:
                maxval = i_max
        self.maxval = maxval
        self.n_atoms = n_atoms
        print('Maximum local current value', self.maxval)

        self.lcurrents = (positions, neighs, currents)
        return


    def drop(self, relcurr = None, abscurr = None, distance = None):
        """
        Drop local currents based on one or more of the conditions:
        relcurr: percentage with respect to maximum local current value
        abscurr: values below a given quantity
        distance: distance between neighbor (in A) larger than a given value
        """
        positions = self.lcurrents[0]
        neighs = self.lcurrents[1]
        currents = self.lcurrents[2]
        maxval = self.maxval
        n_atoms = self.n_atoms
        if relcurr is not None:
            relval = self.maxval * relcurr
        for atm in range(n_atoms):
            to_be_removed = []
            for ineigh in range(len(currents[atm])):
                if distance is not None:
                    if np.linalg.norm(positions[atm] - positions[ineigh]) > distance:
                        to_be_removed.append(ineigh)
                if abscurr is not None:
                    if abs(currents[atm][ineigh]) < abscurr:
                        to_be_removed.append(ineigh)
                if relcurr is not None:
                    if abs(currents[atm][ineigh]/maxval) < relcurr:
                        to_be_removed.append(ineigh)
            currents[atm] = np.delete(currents[atm], to_be_removed)
            neighs[atm] = np.delete(neighs[atm], to_be_removed)
        return


    def print_info(self):
        """
        Print out some infos about the local currents
        """
        assert(self.lcurrents is not None)
        positions = self.lcurrents[0]
        neighs = self.lcurrents[1]
        currents = self.lcurrents[2]
        n_atoms = self.n_atoms
        maxval = 0.0
        minval = 1e10
        for atom in range(n_atoms):
            if len(currents[atom]) == 0:
                continue
            i_max = abs(currents[atom]).max()
            i_min = abs(currents[atom]).min()
            if i_max > maxval:
                maxval = i_max
            if i_min < minval:
                minval = i_min
        print('Maximum local current value', maxval)
        print('Minimum local current value', minval)
        print('Stored maximum value', self.maxval)


    def build_vectors(self):
        """
        Build bond currents and atom currents vectors.
        Local currents are assumed to be vectors along the bond direction.
        In the atom currents representation all the bond currents are summed up.

        n vectors (bond current) or 1 vector (atom current) for each atom are defined
        """
        positions = self.lcurrents[0]
        neighs = self.lcurrents[1]
        currents = self.lcurrents[2]
        n_atoms = self.n_atoms
        bond_currents = []
        atom_currents = []
        for ind in range(n_atoms):
            atom_vec = np.zeros(3)
            bond_currents.append([])
            atom_currents.append([])
            for ineigh, neigh in enumerate(neighs[ind]):
                dr = positions[neigh] - positions[ind]
                dr = dr / np.linalg.norm(dr)
                if currents[ind][ineigh] > 0.0:
                    dr = dr * currents[ind][ineigh]
                    atom_vec += dr
                    bond_currents[ind].append(dr)
            atom_currents[ind].append(atom_vec)
        self.bond_currents = bond_currents
        self.atom_currents = atom_currents


    def save_jmol(self, filename='lc.jmol', scale = 'width', mode = 'bond', ref_length = 2.0, ref_width = 0.2):
        """
        Print out a jmol script
        """
        #Convertion factor from a.u. to Angstrom
        ang = 0.529177211
        positions = self.lcurrents[0]
        if self.bond_currents is None or self.atom_currents is None:
            self.build_vectors()
        n_atoms = self.n_atoms
        arrow_num = 0
        with open(filename, 'w') as jmolfile:
            if mode == 'bond':
                vector_list = self.bond_currents
            elif mode == 'atom':
                vector_list = self.atom_currents
            else:
                raise ValueError('mode must be either bond or atom')
            for ind in range(n_atoms):
                for vector in vector_list[ind]:
                    # Scale current vector to maximum value
                    vector = vector / self.maxval
                    amplitude = np.linalg.norm(vector)
                    length = ref_length
                    width = ref_width
                    if scale == 'width':
                        width *= amplitude
                    elif scale == 'length':
                        length *= amplitude
                    else:
                        raise ValueError('Scale must be either ''width'' or ''length'' ')
                    arrow_num += 1
                    print('vector',vector)
                    output_string = ' #\t' + str(ind) + '\t' \
                                    + '\t' + str(amplitude) + '\n'
                    output_string += 'draw arr' + str(arrow_num) + \
                                     ' arrow width ' + str(width) + \
                                     ' {\t' + str(ang*positions[ind][0]) + \
                                     '\t' + str(ang*positions[ind][1]) + '\t' + \
                                     str(ang*positions[ind][2]) + '}{\t' + \
                                     str(ang*vector[0]*length + ang*positions[ind][0]) + \
                                     '\t' + str(ang*vector[1]*length + ang*positions[ind][1]) + \
                                     '\t' + str(ang*vector[2]*length + ang*positions[ind][2]) + \
                                     '} color red\n'
                    jmolfile.write(output_string)


    def save_vtk(self, filename = 'tmp.vtk'):
        """
        Create a VTK unstructured grid file
        NOTE: only makes sense for atom currents, we can not assign two vectors
        on the same point, as it is not a well defined vector field
        """
        ang = 0.529177211
        positions = self.lcurrents[0]
        if self.bond_currents is None or self.atom_currents is None:
            self.build_vectors()
        currents = self.atom_currents
        print('currents', currents)
        with open(filename, 'w') as outfile:
            #REMINDER: in cube file negative voxel length means Angstrom Units
            outfile.write('# vtk DataFile Version 3.0 \n')
            outfile.write('vtk output \n')
            outfile.write('ASCII\n')
            outfile.write('DATASET UNSTRUCTURED_GRID\n')
            outfile.write('POINTS {} float\n'.format(self.n_atoms))
            for ind in range(self.n_atoms):
                outfile.write('{} {} {}\n'.format(ang*positions[ind][0],
                                                  ang*positions[ind][1],
                                                  ang*positions[ind][2]))
            outfile.write('\nCELLS {} {}\n'.format(self.n_atoms, 2*self.n_atoms))
            for ind in range(self.n_atoms):
                outfile.write('1 {}\n'.format(ind))
            outfile.write('\nCELL_TYPES {}\n'.format(self.n_atoms))
            for ind in range(self.n_atoms):
                outfile.write('1\n'.format())
            outfile.write('\nPOINT_DATA {}\n'.format(self.n_atoms))
            outfile.write('VECTORS local_current float\n'.format())
            for ind in range(self.n_atoms):
                outfile.write('{} {} {}\n'.format(currents[ind][0][0],
                                                  currents[ind][0][1],
                                                  currents[ind][0][2]))


