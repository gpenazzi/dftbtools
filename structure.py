import sys
import ase
import numpy as np


def build_wire(unit_cell, configuration = 1, 
               direction = 0, contact_pl_conf = 1):
    """Build a structure suitable for DFTB+ NEGF transport calculation by 
    repetition of unit cells. 
    
    Different unit cells can be used, the only constrain is that the 
    lattice vector along the transport direction must be the same.
    Pattern and substitution can be conveniently defined.
    
    Args:
       unit_cell (ase.atoms.Atoms or list):
           
           structure containing the unit cell needed. If a single file is 
           specified, the function will build a uniform wire. If a list 
           of files is specified, all of them must have the same
           periodicity along transport direction. A list of structures
           can be used, in this case the configuration will be built using 
           "configuration" option
           
        configuration (integer or list):
            
            if a single integer n is specified, a uniform wire with n
            layers is built. If a list is specified, the configuration 
            will be built according to the indexes specified in the list.
            The indexes specify which unit_cell will be used.
            
            Ex. configuration = (1, 0, 2, 0) will glue together
            unit_cell[1], unit_cell[0], unit_cell[2], unit_cell[0]
            
        direction (integer):
            
            specify transport direction (0, 1 or 2). The corresponding
            periodicity vector will be used.
             
        contact_pl_conf (integer or list):
            
            contacts are built from unit_cell. A contact is made 
	    of 2 PLs. This integer or list specifies how to build a
         contact PL with the same logic of <configuration>. If an integer n is
         specified, n repetition of unit_cell[0] will be used.
            

    Returns:
       ase.atom.Atoms.  Return a structure useful as a DFTB+ NEGF input
       ase.atom.Atoms.  the device region
       ase.atom.Atoms.  the contact 1
       ase.atom.Atoms.  the contact 2
       list             integer specifying the PL indexes
    """

    #We work anyway with list of unit cells   
    #TODO: make a better chck on the type, raising exception where needed
    cells = unit_cell
    if type(unit_cell) == ase.atoms.Atoms:
        cells = [unit_cell]
        
    
    for cell in cells:    
        if not cell.get_pbc()[direction]:
            error_string = 'Error: unit_cell is not periodic in '
            + 'transport direction'
            sys.exit(error_string)
            
    #We always work with <configuration> as a list
    if type(configuration) == int:
        tmp = [0] * configuration
        configuration = tmp
        
        #We always work with <configuration> as a list
    if type(contact_pl_conf) == int:
        tmp = [0] * contact_pl_conf
        contact_pl_conf = tmp
            
    translation = cells[0].get_cell()[direction]
    
    #Build contact 1, pointing opposite to <translation>
    contact_pl = ase.atoms.Atoms()
    #Note: we want to leave slot 0 for the device
    translation = np.array((0.0, 0.0, 0.0))
    for i, conf in enumerate(contact_pl_conf):
        tmp = cells[conf].copy()
        translation += tmp.get_cell()[direction] * (-1.0)
        tmp.translate(translation)
        contact_pl += tmp

    contact_1 = contact_pl.copy()
    contact_pl.translate(translation)       
    contact_1 += contact_pl
        
    
    #Build up device area
    scattering_region = ase.atoms.Atoms()
    translation = np.array((0.0, 0.0, 0.0))
    for i, conf in enumerate(configuration):
        tmp = cells[conf].copy()
        tmp.translate(translation)
        scattering_region += tmp
        translation += tmp.get_cell()[direction]
        
    scattering_region.set_pbc(True)
    scattering_region_cell_vectors = cells[0].get_cell()
    scattering_region_cell_vectors[direction] = (translation)
    scattering_region.set_cell(scattering_region_cell_vectors)
    

    #Build contact 2, pointing in the direction of <translation>
    contact_pl = ase.atoms.Atoms()
    c2_translation = np.array((0.0, 0.0, 0.0))
    for i, conf in enumerate(contact_pl_conf):
        tmp = cells[conf].copy()
        tmp.translate(translation)
        contact_pl += tmp
        translation += tmp.get_cell()[direction]
        c2_translation += tmp.get_cell()[direction]

    contact_2 = contact_pl.copy()
    contact_pl.translate(
    c2_translation)
    contact_2 += contact_pl    
    
    indexes = list()
    print('Device Area: ', 1, ' ', len(scattering_region))
    indexes.append(1)
    tmp = len(scattering_region) + 1
    indexes.append(tmp - 1)
    print('Contact 1: ', tmp, ' ', tmp + len(contact_1) - 1)
    indexes.append(tmp)
    tmp = len(scattering_region) + 1 + len(contact_1)
    indexes.append(tmp - 1)
    print('Contact 1: ', tmp, ' ', tmp + len(contact_2) - 1)
    indexes.append(tmp)
    indexes.append(tmp + len(contact_2) - 1)

        
    device = ase.atoms.Atoms()
    device += scattering_region
    device += contact_1
    device += contact_2
        
    #Finished, returning structure
    return device, scattering_region, contact_1, contact_2, indexes


def order(structure, direction, cutoff = 12.0,
        indexing = 'Fortran'):
    """Reorder an ASE atoms structure along a given direction. Returns a copy of
    the ordered structure and a list of Principal Layer indexes. """

    shift = 0
    if indexing == 'Fortran':
        shift = 1

    swap = structure.positions[:,direction].argsort()
    ordered = structure[swap]
    positions = ordered.positions[:,direction]
    minval = np.amin(positions)
    pls = [0]
    
    comp = [False]
    N = 0
    while not all(comp):
        N += 1
        comp = positions < (minval + N*cutoff)
        ntrues = np.count_nonzero(comp)
        #Check if all True are contiguous (consistency check)
        if not all(comp[:ntrues]):
            raise ValueError('Structure ordering went wrong: consistency failed')
        if N>1e7:
            raise ValueError('Structure ordering went wrong: loop too long')
        pls.append(ntrues)

    #Note: the last value is just the total number of atoms. The value before is 
    # not considered as it is generally not safe (last layer
    #is always smaller than cutoff)
    pls = np.array(pls[:-2]) + shift

    return (ordered.copy(), pls)
