"""Routines to parse formatted matrices as read from DFTB and Libnegf output"""
import numpy as np
import scipy.sparse

def read_zcsr(filename):
    """Read formatted CSR as written by libnegf and return the corresponding
    arrays. Note: all the indexes are converted to C indexing, i.e. starting
    from 0"""

    with open(filename, 'r') as infile:
        lines = infile.readlines()
        
        if not 'Nrow' in lines[0]:
            raise IOError('Expected Nrow in first line')
        nrow = int(lines[0].split()[1])
        
        if not 'Ncol' in lines[1]:
            raise IOError('Expected Ncol in second line')
        ncol = int(lines[1].split()[1])
        
        if not 'Nnz' in lines[2]:
            raise IOError('Expected Nnz in third line')
        nnz = int(lines[2].split()[1])
        print('nnz', nnz)
        
        if not 'Nzval array' in lines[3]:
            raise IOError('Cannot find string Nzval array')
        nzval = np.zeros(nnz, dtype=complex)
        for ind in range(4, 4 + nnz):
            nzval[ind - 4] = complex(
            float(lines[ind].replace(' ','').strip('() \n').split(',')[0]), 
            float(lines[ind].replace(' ','').strip('() \n').split(',')[1]))
        
        if not 'Colind array' in lines[4 + nnz]:
            raise IOError('Cannot find string Colind array')
        colind = np.zeros(nnz, dtype=int)
        for ind in range(5 + nnz, 5 + nnz + nnz):
            colind[ind - 5 - nnz] = int(lines[ind].strip(' \n')) - 1
        
        rowpnt = np.zeros(nrow, dtype=int)
        if not 'Rowpnt array' in lines[5 + nnz + nnz]:
            raise IOError('Cannot find string Colind array')
        for ind in range(6 + nnz + nnz, 6 + nnz + nnz + nrow):
            rowpnt[ind - 6 - nnz - nnz] = int(lines[ind].strip(' \n')) - 1
        mat = scipy.sparse.csr_matrix((nzval, colind, rowpnt), dtype = complex)
        return mat

