import dftbtools.basis as slater_mio
import dftbtools.slater as slater
import numpy as np

symbol = 'C'
ll = 1
mm = -1
orb = slater.SlaterType(ll, mm, slater_mio.coeff[(symbol, ll,
                    'exp')], slater_mio.coeff[(symbol, ll,
                        'pow')],save=('c_1_-1.orb', 'c_1_-1.grad'),
                    res = 0.2)
orb._grid.dump_to_vtk(np.real(orb._so_grid), 'c_1_-1.real.vtk')
orb._grid.dump_to_vtk(np.imag(orb._so_grid), 'c_1_-1.imag.vtk')
